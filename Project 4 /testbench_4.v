module testbench_4;

wire [63:0] adder_out;
wire [63:0] pc_out;
wire [63:0] ins_memory_out;

adder adder1(.data_one(pc_out), .data_two(64'b100), adder_out);

pc pc1(.data(adder_out), .out(pc_out));

instructionmemory ins_mem(.address(pc_out), .ins_out(ins_memory_out);

endmodule