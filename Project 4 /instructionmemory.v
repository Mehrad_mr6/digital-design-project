module instructionmemory(address, ins_out);
integer i;

input[63:0] address;
output [63:0] ins_out;

reg[n-1:0] ram [0:255];

initial 
  begin
    for(i = 0; i < 256; i = i + 1)
       ram[i] = i;  
  end
begin
  //r-type
   ram[0] = 64'b00110011;
  //sd
  ram[1] = 64'b01000111;
  //ld
  ram[2]= 64'b00000011;
  //beq
  ram[3]= 64'b01100011;
end
endmodule;