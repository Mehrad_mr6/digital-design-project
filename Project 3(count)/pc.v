module pc(data, out);

input [63:0] data;
output reg [63:0] out;

always
begin
  out = data;
end

endmodule;