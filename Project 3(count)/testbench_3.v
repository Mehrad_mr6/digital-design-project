module testbench_3;

wire [63:0] adder_out;

wire [63:0] pc_out;

adder adder1(.data_one(pc_out), .data_two(64'b100), adder_out);

pc pc1(.data(adder_out), .out(pc_out));

endmodule