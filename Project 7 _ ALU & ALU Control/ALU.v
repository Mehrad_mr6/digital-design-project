module alu(a, b, c, res, zero);
input [63:0] a, b;
input [3:0] c;
output [63:0] res;
output zero;

reg [63:0] res_temporary;
reg zero_temporary;

always@(*) begin
    zero_temporary = 0;
    case(c)
        4'b0000: #5 res_temporary = a & b;
  4'b0001: #5 res_temporary = a | b;
  4'b0010: #5 res_temporary = a + b;
  4'b0110: #5 res_temporary = a - b;
    endcase

    if(res_temporary[63:0] == 0) begin
      zero_temporary = 1;
    end

end
assign res = res_temporary[63:0];
assign zero = zero_temporary;
endmodule;
